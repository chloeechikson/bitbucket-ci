import { createElement } from "lwc";
import Counter from "c/counter";

describe("counter", () => {
  afterEach(() => {
    // The jsdom instance is shared across test cases in a single file so reset the DOM
    while (document.body.firstChild) {
      document.body.removeChild(document.body.firstChild);
    }
  });

  it("displays 0 on load", () => {
    const element = createElement("counter", {
      is: Counter
    });
    document.body.appendChild(element);

    const count = element.shadowRoot.querySelector("h1");
    expect(count.textContent).toEqual("0");
  });

  it("displays 1 after clicking the increment button once", async () => {
    const element = createElement("counter", {
      is: Counter
    });
    document.body.appendChild(element);

    const button = element.shadowRoot.querySelector("lightning-button-icon");
    button.click();

    await Promise.resolve();

    const count = element.shadowRoot.querySelector("h1");
    expect(count.textContent).toEqual("1");
  });

  it("displays 5 after clicking the increment button five times", async () => {
    const element = createElement("counter", {
      is: Counter
    });
    document.body.appendChild(element);

    const button = element.shadowRoot.querySelector("lightning-button-icon");
    button.click();
    button.click();
    button.click();
    button.click();
    button.click();

    await Promise.resolve();

    const count = element.shadowRoot.querySelector("h1");
    expect(count.textContent).toEqual("5");
  });
});
