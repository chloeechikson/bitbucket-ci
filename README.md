# Simple SFDX CI

A simple pipeline for linting and testing LWC

## Helpful Resources

[Setup SFDX](https://developer.salesforce.com/docs/component-library/documentation/en/lwc/lwc.install_setup_develop)
[Learn git](https://www.atlassian.com/git)
[Set up multiple SSH Keys for bitbucket](https://bitbucket.org/blog/better-support-multiple-ssh-keys)
[Apex debugger](https://trailhead.salesforce.com/content/learn/projects/find-and-fix-bugs-with-apex-replay-debugger)
